# rust-new

An interactive CLI tool to create new Rust projects.

Example dialogue:

```text
Creating new Rust project
=========================
Current settings:
* project type:           application
* project's name:         tester
* project's path:         Tester
* project's description:  A simple test project.
Are these values correct? [Y/n]:
A folder already exists at "Tester".  Should it be removed? [y/N]: y
Successfully created tester.
```

## License

Licensed under either of

* Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or https://opensource.org/licenses/Apache-2.0)
* MIT License ([LICENSE-MIT](LICENSE-MIT) or https://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the
work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
