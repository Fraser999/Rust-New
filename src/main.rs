//! An interactive CLI tool to create new Rust projects.

#![doc(test(attr(forbid(warnings))))]
#![warn(unused, missing_copy_implementations, missing_docs)]
#![deny(
    deprecated_in_future,
    future_incompatible,
    macro_use_extern_crate,
    rust_2018_idioms,
    nonstandard_style,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_lifetimes,
    unused_qualifications,
    unused_results,
    warnings,
    clippy::all,
    clippy::pedantic
)]
#![forbid(
    const_err,
    duplicate_macro_exports,
    exceeding_bitshifts,
    invalid_type_param_default,
    legacy_constructor_visibility,
    legacy_directory_ownership,
    macro_expanded_macro_exports_accessed_by_absolute_paths,
    missing_fragment_specifier,
    mutable_transmutes,
    no_mangle_const_items,
    order_dependent_trait_objects,
    overflowing_literals,
    parenthesized_params_in_types_and_modules,
    pub_use_of_private_extern_crate,
    safe_extern_statics,
    unknown_crate_types
)]
#![allow()]

/// Module for creating project files.
mod file_contents;
/// Reads and validates input from a stream.
mod input_getter;
/// Main struct which holds the requested settings for the new project along with functions to
/// modify these and generate the new project's folders and files.
mod project;

use project::Project;

/// The main entry-point.
fn main() {
    let mut project = Project::default();
    while !project.approve_settings() {
        project.get_settings();
    }
    while !project.check_and_clear_path() {
        project.get_path();
    }
    project.create();
    project.open();
}
